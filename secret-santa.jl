import Permutations
import TOML

"""
    prepare_mails(fn="players.toml")

Randomly chose a *victim* for each player specified in the TOML file that `fn` points to
and prepare an E-mail message from your (currently actually my) account configured for
`himalaya` to their E-mail adress specified in the TOML file.
The message informs them about their *victim* and their mission.

The TOML file located at `fn` is expected to contain an entry `players` holding a list of
objects with the following keys:

- `name` (String)
- `full_name` (String)
- `mail` (String)
- `sex` (`"f"` or `"m"`)
- `not` (Optional List of Strings with names of players not to choose)

An example is given below.

```toml
players = [
    { name = "Alice", full_name = "Alice A.", mail = "alice.a@cosmos.org", sex = "f" },
    { name = "Bob", full_name = "Bob B.", mail = "bob.b@milkyway.com", sex = "m", not = ["Alice"] },
]
```
"""
function prepare_mails(fn=joinpath(@__DIR__, "players.toml"))
    input = TOML.parsefile(fn)
    players = get(input, "players", Dict{String,String}[])
    n = length(players)
    cancelled = exclusions(players)
    perm = shuffle(n, cancelled)
    for (i, p) in enumerate(players)
        write_mail(p, players[perm(i)])
    end
end

"""
    exclusions(players)

Get a list of integer pairs in the form of `Vector{Tuple{Int,Int}}` holding the indices of
players and corresponding indices of players they do not want to choose.
"""
function exclusions(players)
    cancelled = Tuple{Int,Int}[]
    for (i, player) in enumerate(players)
        for other in get(player, "not", String[])
            j = findfirst(p -> p["name"] == other, players)
            if !isnothing(j)
                push!(cancelled, (i, j))
            end
        end
    end
    return cancelled
end

"""
    shuffle(n, cancelled)

Get a random permutation of `n` players without fixed points and without the connections
listed in `cancelled`.
"""
function shuffle(n, cancelled)
    perm = Permutations.RandomPermutation(n)
    iscancelled(perm, cancelled) || return perm
    return shuffle(n, cancelled)
end

"""
    iscancelled(perm, cancelled)

Check if `perm` is cancelled due to containing either a fixed point or a connection in
`cancelled`.
"""
function iscancelled(perm, cancelled)
    isempty(Permutations.fixed_points(perm)) || return true
    for (i, j) in cancelled
        perm(i) == j && return true
    end
    return false
end

"""
    write_mail(player, victim)

Write an E-mail message to `"mails/$(player["name"]).eml"` informing `player` about their
`victim`.
"""
function write_mail(player, victim)
    mkpath(joinpath(@__DIR__, "mails"))
    dear = Dict("f" => "Liebe", "m" => "Lieber")
    her = Dict("f" => "ihr", "m" => "ihm")
    open(joinpath(@__DIR__, "mails", "$(player["name"]).eml"), "w") do io
        print(
            io,
            """
            From: Daniel Schwabeneder <daschw@disroot.org>
            To: $(player["full_name"]) <$(player["mail"])>
            Subject: 🎄🎄🎄 WICHTELIG 🎄🎄🎄 Weihnachtswichteln im Hause Schwabeneder

            $(dear[player["sex"]]) $(player["name"]),

            es wird wieder gewichtelt! Du darfst heuer

                🎉🎉🎉 $(victim["name"]) 🎉🎉🎉

            beschenken. Bitte schau, dass du $(her[victim["sex"]]) eine Freude machst und
            das am Besten mit nicht mehr als 50 EUR!
            Ich wünsche dir dabei viel Erfolg!

            Liebe Grüße,
            Dani

            P.S.: Das ist eine automatisch erzeugte E-mail, die auch gleich automatisch aus
            meinem Postausgang gelöscht wurde. Bitte antworte NICHT auf diese Nachricht,
            damit ich nicht weiß, wen du gezogen hast!
            """
        )
    end
end

prepare_mails()
