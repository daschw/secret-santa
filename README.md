# secret-santa

The code in this repository performs the following steps:
1. It assigns secret santa gift recievers to players defined in a `players.toml` file in the
   root of the repository.
   The `players.toml` configuration file has to be created manually locally and is expected
   to have the following structure:

   ```toml
    players = [
        { name = "Alice", full_name = "Alice A.", mail = "alice.a@cosmos.org", sex = "f" },
        { name = "Bob", full_name = "Bob B.", mail = "bob.b@milkyway.com", sex = "m", not = ["Alice"] },
    ]
   ```

2. It creates e-mail messages for all players defined in `players.toml` imforming them about
   their corresponding gift receivers.
   To assign the gift receivers and create the e-mail messages
   [julia](https://julialang.org)
   is expected to be installed on the system.
3. It sends the e-mails to the players defined in `players.toml` using
   [himalaya](https://github.com/soywod/himalaya).
   It expects a default account to be set up for `himalaya`.
4. It deletes the e-mail messages from the `Sent` folder and from the file system.
   For this step [jaq](https://github.com/01mf02/jaq) is required.

## Requirements

- [julia](https://julialang.org)
- [himalaya](https://github.com/soywod/himalaya) with a working account
- [jaq](https://github.com/01mf02/jaq)
- a `players.toml` file in the root of the direcory matching the structure indicated above.

## Usage

```bash
./secret-santa
```
